entity T07 is
    end entity;
        
architecture sim of T07 is
--  declaritive region
    signal countup : integer := 0;
    signal countdown : integer := 10;
begin
    process is
    begin
        countup <= countup + 1;
        countdown <= countdown - 1;
        wait for 10 ns;
    end process;
    
    process is
    begin
        wait on countup, countdown;
        report "up-"&integer'image(countup) &
            " down-"&integer'image(countdown);
    end process;

    process is
    begin
        wait until countup = countdown;
        report "JACKPOT!";
    end process;
end architecture;