entity T08 is
    end entity;
        
architecture sim of T08 is
--  declaritive region
    signal countup : integer := 0;
    signal countdown : integer := 10;
begin
    process is
    begin
        countup <= countup + 1;
        countdown <= countdown - 1;
        wait for 10 ns;
    end process;
    
    process is
    begin
        if countup > countdown then
            report "countup is greater";
        elsif countup < countdown then
            report "countup is less than";
        else
            report "they are equal";
        end if;
        wait on countup, countdown;
    end process;

end architecture;