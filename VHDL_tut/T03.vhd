entity T03 is
    end entity;
    
architecture sim of T03 is
begin
    process is
    begin
        report "Hello!";
        loop
            report "Peakaboo!";
            exit;
        end loop;
        report "Goodbye";
        wait;
    end process;
end architecture;