library ieee;
use ieee.std_logic_1164.all;

entity T010 is
end entity;
        
architecture sim of T010 is
--  declaritive region
--  standard logic is like a singel stream of wire
    signal signal1 : std_logic := '0';
    signal signal2 : std_logic;
    signal signal3 : std_logic;

begin
    process is
    begin

        wait for 10 ns;
        --  operator on the bits
        signal1 <= not signal1;
    end process;
    
  
end architecture;