entity T09 is
    end entity;
        
architecture sim of T09 is
--  declaritive region
    signal countup : integer := 0;
    signal countdown : integer := 10;
begin
    process is
    begin
        countup <= countup + 1;
        countdown <= countdown - 1;
        wait for 10 ns;
    end process;
    
    process is
    begin
        if countup = countdown then
            report "process A: jackpot";
        end if;
        wait on countup, countdown;
    end process;
--  logically equivalent the below is a sensitivity list
    process(countup, countdown) is
        begin
            if countup = countdown then
                report "process B: jacpot!";
            end if;
        end process;
end architecture;