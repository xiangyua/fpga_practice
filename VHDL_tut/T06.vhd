entity T06 is
end entity;
    
architecture sim of T06 is
--  declaritive region
    signal mysignal : integer := 0;
begin
    process is
        variable myvariable : integer := 0;
    begin
        myvariable := myvariable + 1;
        -- signal assignment
        mysignal <= mysignal + 1;

        report "var-"&integer'image(myvariable) & " sig-"&integer'image(mysignal);
        wait for 10 ns;
    end process;
end architecture;